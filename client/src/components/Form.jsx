import React from 'react';
import { useState } from 'react';
import FormInput from './FormInput';
import FormList from './FormList';
import { Container, Row, Col } from 'react-bootstrap';
import FormEdit from './FormEdit';

function Form() {
  const [users, setUsers] = useState([
    {
      id: 1,
      username: 'chinez',
      password: '0987654321',
      email: 'chinez@gmail.com',
    },
    {
      id: 2,
      username: 'chine',
      password: '09876543',
      email: 'chine@gmail.com',
    },
    {
      id: 3,
      username: 'chi',
      password: '7654321',
      email: 'chi@gmail.com',
    },
  ]);
  const [currentUser, setCurrentUser] = useState({
    id: null,
    username: '',
    password: '',
    email: '',
  });
  const [edit, setEdit] = useState(false);

  const addUser = (user) => {
    user.id = users.length + 1;
    setUsers([...users, user]);
    console.log(users);
  };
  const editUser = (user) => {
    setEdit(true);
    setCurrentUser({
      id: user.id,
      username: user.username,
      password: user.password,
      email: user.email,
    });
    console.log(users);
  };
  const updateUser = (id, updateUser) => {
    setEdit(false);
    setUsers(users.map((user) => (user.id === id ? updateUser : user)));
    console.log(currentUser, 'ini current user');
  };
  const deleteUser = (id) => {
    setUsers(users.filter((user) => user.id !== id));
  };
  return (
    <>
      <Container>
        <Row className="mt-5">
          <Col>
            {edit ? (
              <>
                <h2>Edit user</h2>
                <FormEdit
                  edit={edit}
                  setEditing={setEdit}
                  currentUser={currentUser}
                  updateUser={updateUser}
                />
              </>
            ) : (
              <>
                <h2>add User</h2>
                <FormInput addUser={addUser} />
              </>
            )}
          </Col>
          <Col md={8}>
            <FormList
              users={users}
              deleteUser={deleteUser}
              editUser={editUser}
            />
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Form;
