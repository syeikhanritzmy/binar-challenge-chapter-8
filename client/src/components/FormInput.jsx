import React from 'react';
import { useState } from 'react';
import { Form, Button, Row, Col, Stack } from 'react-bootstrap';
function FormInput({ addUser }) {
  const [user, setUser] = useState({
    id: null,
    username: '',
    password: '',
    email: '',
  });

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setUser({ ...user, [name]: value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    addUser(user);
    setUser({
      username: '',
      password: '',
      email: '',
    });
    console.log(user);
  };
  return (
    <>
      <Form onSubmit={handleSubmit}>
        <Row>
          <Stack gap={3}>
            <Form.Control
              name="username"
              placeholder="username"
              type="text"
              value={user.username}
              onChange={handleChangeInput}
            />
            <Form.Control
              name="password"
              placeholder="password"
              type="password"
              value={user.password}
              onChange={handleChangeInput}
            />
            <Form.Control
              name="email"
              placeholder="email"
              type="email"
              value={user.email}
              onChange={handleChangeInput}
            />

            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Stack>
        </Row>
      </Form>
    </>
  );
}

export default FormInput;
