import React from 'react';
import { Stack, Table, Button } from 'react-bootstrap';

function FormList({ users, deleteUser, editUser }) {
  return (
    <>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>No</th>
            <th>Username</th>
            <th>Password</th>
            <th>Email</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr key={user.id}>
              <td>{index + 1}</td>
              <td>{user.username}</td>
              <td>{user.password}</td>
              <td>{user.email}</td>
              <td>
                <Stack
                  direction="horizontal"
                  gap={2}
                  className="mx-auto col-md-7"
                >
                  <Button
                    variant="warning"
                    onClick={() => {
                      editUser(user);
                    }}
                  >
                    EDIT
                  </Button>
                  <Button
                    variant="danger"
                    onClick={() => {
                      deleteUser(user.id);
                    }}
                  >
                    DELETE
                  </Button>
                </Stack>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}

export default FormList;
