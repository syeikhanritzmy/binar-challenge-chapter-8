{
  "swagger": "2.0",
  "info": {
    "description": "Documentation RESTAPI",
    "title": "Chapter 8",
    "version": "1.0.0"
  },
  "basePath": "/api/players",
  "tags": [
    {
      "name": "player",
      "description": "player Resource"
    }
  ],
  "paths": {
    "/players": {
      "get": {
        "tags": ["player"],
        "summary": "List All Players",
        "description": "Description for list players",
        "produces": ["application/json"],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "username": {
                    "type": "string",
                    "example": "Luffy"
                  },
                  "email": {
                    "type": "string",
                    "example": "Luffy@gmail.com"
                  },
                  "experience": {
                    "type": "integer",
                    "example": 6000
                  },
                  "lvl": {
                    "type": "integer",
                    "example": 600
                  }
                }
              }
            }
          },
          "500": {
            "description": "some error occured while retrieving players"
          }
        }
      },
      "post": {
        "tags": ["player"],
        "summary": "Add new Player",
        "description": "Create new Player",
        "consumes": ["application/json"],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Player Object",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "username": {
                  "type": "string",
                  "example": "Luffy"
                },
                "email": {
                  "type": "string",
                  "example": "luffy@gmail.com"
                },
                "password": {
                  "type": "string",
                  "example": "luffy12345"
                },
                "experience": {
                  "type": "integer",
                  "example": 300
                }
              }
            }
          }
        ],
        "produces": ["application/json"],
        "responses": {
          "201": {
            "description": "Success",
            "schema": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer",
                  "example": 1
                },
                "username": {
                  "type": "string",
                  "example": "luffy"
                },
                "email": {
                  "type": "string",
                  "example": "luffy@gmail.com"
                },
                "password": {
                  "type": "string",
                  "example": "luffy12345"
                },
                "experience": {
                  "type": "integer",
                  "example": 300
                }
              }
            }
          },
          "400": {
            "description": "failed",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "username or email cannot empty"
                }
              }
            }
          },
          "500": {
            "description": "failed",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Some error occured while creating the player"
                }
              }
            }
          }
        }
      },

      "/exp/{playerId}": {
        "post": {
          "tags": ["player"],
          "summary": "update Experience player",
          "description": "this can only be done the logged in user",
          "consumes": ["application/json"],
          "parameters": [
            {
              "in": "body",
              "name": "body",
              "description": "update experience",
              "required": true,
              "schema": {
                "type": "object",
                "properties": {
                  "experience": {
                    "type": "integer",
                    "example": 55000
                  }
                }
              }
            }
          ],
          "produces": ["application/json"],
          "responses": {
            "200": {
              "description": "Success",
              "schema": {
                "type": "object",
                "properties": {
                  "message": {
                    "type": "string",
                    "example": "PLayer with id = {id} gain {exp} experience, total experience = {player.experience}"
                  }
                }
              }
            },
            "400": {
              "description": "Failed",
              "schema": {
                "type": "object",
                "properties": {
                  "message": {
                    "type": "string",
                    "example": "Cannot update player with id={id}"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/{playerId}": {
      "get": {
        "tags": ["player"],
        "summary": "Find Player By ID",
        "description": "Returns a single Player",
        "parameters": [
          {
            "in": "path",
            "name": "playerId",
            "description": "ID of player to return",
            "required": true,
            "type": "integer"
          }
        ],
        "produces": ["application/json"],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "integer",
                  "example": 1
                },
                "username": {
                  "type": "string",
                  "example": "luffy"
                },
                "email": {
                  "type": "string",
                  "example": "luffy@gmail.com"
                },
                "experience": {
                  "type": "integer",
                  "example": 60000
                },
                "lvl": {
                  "type": "integer",
                  "example": 600
                }
              }
            }
          },
          "400": {
            "description": "Player not Found",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Player with {id} not Found"
                }
              }
            }
          }
        }
      },
      "put": {
        "tags": ["player"],
        "summary": "Update player",
        "description": "update data player",
        "produces": ["application/json"],
        "parameters": [
          {
            "in": "path",
            "name": "playerId",
            "description": "Player update",
            "required": true,
            "type": "integer"
          },
          {
            "in": "body",
            "name": "body",
            "description": "update user object",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "username": {
                  "type": "string",
                  "example": "luffy"
                },
                "email": {
                  "type": "string",
                  "example": "luffy@gmail.com"
                },
                "password": {
                  "type": "string",
                  "example": "luffy12345"
                },
                "experience": {
                  "type": "integer",
                  "example": 500
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success Update",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "PLayer with id: {id} successfully update"
                }
              }
            }
          },
          "400": {
            "description": "Player Not Found",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "PLayer with id: {id} not found"
                }
              }
            }
          },
          "500": {
            "description": "Failed",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Failed to update"
                }
              }
            }
          }
        }
      },
      "delete": {
        "tags": ["player"],
        "summary": "Delete Player",
        "description": "Delete user permanent with id",
        "produces": ["application/json"],
        "parameters": [
          {
            "name": "playerId",
            "in": "path",
            "description": "the user taht needs to be deleted",
            "required": true,
            "type": "integer"
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "PLayer with id: {id} deleted successfully"
                }
              }
            }
          },
          "400": {
            "description": "Failed",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Cannot delete Player with id: {id}, maybe player was not found !"
                }
              }
            }
          }
        }
      }
    }
  }
}
